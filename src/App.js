import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Books from './Books.js'

class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Books></Books>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
